require 'sinatra'
require 'mysql2'
require 'yaml'
require 'connection_pool'

get '/' do
  @hostname = Socket.gethostname
  settings.mysql.with do |mysql|
  	@hits = mysql.query("SELECT * FROM hits ORDER BY id DESC LIMIT 20")
  	mysql.query("INSERT INTO hits (`user_agent`, ip) VALUES ('#{mysql.escape(request.user_agent)}', '#{request.ip}')")
  end
  erb :main
end

configure do |c|
	mysql_config = YAML.load_file(::File.join("config", "database.yml"))
	set :mysql, ConnectionPool.new(:size => 20, :timeout => 0.2) {Mysql2::Client.new(mysql_config[c.environment.to_s])}
end
