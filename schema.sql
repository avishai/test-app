CREATE TABLE IF NOT EXISTS hits (
    id INT AUTO_INCREMENT PRIMARY KEY,
    `user_agent` varchar(100),
    ip varchar(15)
);
